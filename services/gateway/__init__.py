from flask import Flask, request
from flask_nameko import FlaskPooledClusterRpcProxy

config = {
    'NAMEKO_AMQP_URI': "amqp://guest:guest@rabbitmq"
}
app = Flask(__name__)
rpc = FlaskPooledClusterRpcProxy()
rpc.config.update(config)
rpc.init_app(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=80)
